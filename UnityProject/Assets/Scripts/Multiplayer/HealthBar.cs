﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class HealthBar : NetworkBehaviour
{
    public Image currentHealthbar;
    public Text ratioText;

    public int logtime = 120;
    [SyncVar]
    float syncedHealth;

    private float hitpoint = 100;
    private float maxHitpoint = 100;

    public void Start()
    {
        UpdateHealthbar();
    }

    void Update()
    {
        hitpoint = maxHitpoint;
    }

    void FixedUpdate()
    {
        if (isLocalPlayer)
        {
            TransmitHealth();
        }
    }

    public void UpdateHealthbar()
    {
        float ratio = hitpoint / maxHitpoint;
        currentHealthbar.rectTransform.localScale = new Vector3(ratio, 1, 1);
        ratioText.text = (ratio * 100).ToString("0") + "%";
    }

    public void TakeDamage(float damage)
    {
        hitpoint -= damage;
        if (hitpoint <= 0)
        {
            hitpoint = 0;
            Debug.Log(hitpoint);
        }

        UpdateHealthbar();
    }

    public void HealDamage(float heal)
    {
        hitpoint += heal;
        if (hitpoint > maxHitpoint)
        {
            hitpoint = maxHitpoint;
            Debug.Log(hitpoint);
        }


        UpdateHealthbar();
    }

    [Client]
    void TransmitHealth()
    {
        if (hitpoint < maxHitpoint)
        {
            CmdSendHealthToServer(hitpoint);
        }
    }

    [Command]
    void CmdSendHealthToServer(float healthToSend)
    {
        syncedHealth = healthToSend;
    }

}
